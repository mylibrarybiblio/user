package com.library.user.Controller;

import com.library.user.Dto.UserDto;
import com.library.user.Service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api")
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/addUser")
    public ResponseEntity<UserDto> createNewUser(@RequestBody UserDto userDto){
        userService.saveUser(userDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/updateUser")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto){
        userService.updateUser(userDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/deleteUser/{idUser}")
    public ResponseEntity<Void> deleteUser(@PathVariable int idUser){
        userService.deleteUser(idUser);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/allUser")
    public ResponseEntity<List<UserDto>> listUser(){
        List<UserDto> listUserDto = userService.getAllUser();

        return new ResponseEntity<>(listUserDto,HttpStatus.FOUND);
    }

    @GetMapping("/user/{idUser}")
    public UserDto oneUser(@PathVariable int idUser){
        UserDto userDto = userService.getOneUser(idUser);
        return userDto;
    }

    @GetMapping("/searchUser/{wordSearch}")
    public List<UserDto> SearchUser(@PathVariable String wordSearch) {
        List<UserDto> userDtoList = userService.searchUser(wordSearch);
        return userDtoList;
    }
}
