package com.library.user.Service;

import com.library.user.Dto.UserDto;
import com.library.user.Entity.User;

import java.util.List;

public interface IuserService {
    void saveUser(UserDto userDto);
    void updateUser(UserDto userDto);
    void deleteUser(int idUser);
    List<UserDto> getAllUser();
    UserDto getOneUser(int idUser);
    List<UserDto> searchUser(String wordSearch);

}
