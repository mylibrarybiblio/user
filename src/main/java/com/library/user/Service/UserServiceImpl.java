package com.library.user.Service;

import com.library.user.Dto.UserDto;
import com.library.user.Entity.User;
import com.library.user.Exception.UserException;
import com.library.user.Repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements IuserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public void saveUser(UserDto userDto) {
        User userExist = userRepository.findByEmail(userDto.getEmail());
        if (userExist != null) throw new UserException("Cet Utilisateur existe déjà");
        userDto.setCreation_date(new Date());
        User user = convertToEntity(userDto);
        userRepository.save(user);
    }

    @Override
    public void updateUser(UserDto userDto) {
        Optional<User> userExist = userRepository.findById(userDto.getIdUser());
        if (userExist.isEmpty()) throw new UserException("Cet utilisateur n'existe pas");
        User user = convertToEntity(userDto);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(int idUser) {
        User userExist = userRepository.findById(idUser).orElseThrow(()-> new UserException("Cet utilisateur n'existe pas"));
        userRepository.delete(userExist);
    }

    @Override
    public List<UserDto> getAllUser() {
        List<User> userList = userRepository.findAll();
        List<UserDto> d = userList.stream().map(this::convertToDto).collect(Collectors.toList());
        return userList.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public UserDto getOneUser(int idUser) {
        User user = userRepository.findById(idUser).orElseThrow(()->new UserException("Cet utilisateur n'existe pas"));
        UserDto userDto = convertToDto(user);
        return userDto;
    }

    @Override
    public List<UserDto> searchUser(String wordSearch) {
        List<User> userList = userRepository.findByLastNameContains(wordSearch);
        userList.add(userRepository.findByEmail(wordSearch));
        List<UserDto> userDtoList = userList.stream().filter(Objects::nonNull)
                .map(this::convertToDto).collect(Collectors.toList());

        return userDtoList;
    }

    private User convertToEntity(UserDto userDto){
        ModelMapper mapper = new ModelMapper();
        User user = mapper.map(userDto,User.class);
        return user;
    }

    private UserDto convertToDto(User user) {
        ModelMapper mapper = new ModelMapper();
        UserDto userDto = mapper.map(user, UserDto.class);
        return userDto;
    }
}

